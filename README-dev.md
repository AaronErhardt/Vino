## How to build:

### Linux
make sure the build-essential package is installed (for make and g++)  
```bash
cd src/
make #to compile a vino program
make vino # to compile vino itself
```

### Windows  
not available yet

## Files

- __Vino.ino__  
The source file for Vino/Arduino programs. Can be edited with the 'edit' command or any other text editor.

- __arduino.hpp__  
The header linking void setup() and void draw() to the main() function so the Arduino progamm can be executed.

- __main.cpp__  
Contains `int main()`

- __sim_lib.cpp__  
Contains all arduino-specific commands, like `pinMode()` etc. and defines their behavior. Also trying to catch runtime-problems that Arduino programms could possibly contain.

- __sim_util.cpp__  
Defines a lot of functions and objects necessary for the simulation, e.g. an object for the virtual arduino.

- __serial_lib.cpp__  
Defines the object for the serial communication with the virtual Arduino.

- __error.cpp__  
Defines the function `handleError()` that is used in other files to handle errors and warnings.

- __console_util.cpp__  
Contains a lot of useful functions to move the cursor and other utility for the console UI.

- __input.cpp__  
Defines the function `getChar()` that allows you to handle input very smoothly (by using EPOLL).

- __setup_console.cpp__  
Sets up the terminal (deactivates echo, activates direct input etc., "raw" input mode) and activates signal handeling for SIGINT and SIGABRT. Also sets up an EPOLL FD (for input).

- __arduino_constants.hpp__  
Defines all necessary constants like HIGH and LOW or INPUT and OUTPUT.

- __simulation_lib.hpp__  
Combines different headers needed for compiling an Arduino programm with Vino.

- __colors.hpp__  
Defines a lots of colors (with escape sequences) making it easier to use them later.

- __language.hpp__  
Defines strings for language support and easier translation. Later further files like german.hpp will be added to support further languages.

### Other files  

- __makefile__  
Defines all rules for make a utility that prevents files from being compiled again if they or one of their decencies haven't changed.

- __.gitignore__  
specifies files not needed for git.

- __LICENSE__  
GPL v3

### Window-Support (TODO)

- Find a decent c++ compiler, that is easy to install and easy to use (maybe gcc works well on windows as well)

- OPTIONAL: Find a substitute for make, a build tool preventing unnecessary recompilation. Would be nice because the time needed compilation would be much smaller

- Check if escape sequences work on windows, especially those for moving the cursor (console_util.cpp) and 24bit RGB color support (mainly colors.hpp). May be they are now supported on windows 10 since they made WSL (Windows Subsystem for Linux)...

- Find a substitute for unistd.h which is probably not available on windows. However this mainly affects the usleep() function, that is used to create a simple delay in the program. Surly there's something in windows.h.

- Find a replacement for termios.h which enables the "raw" input mode on the console that disables echoing and enables direct input.

- Replace the getChar() function in input.cpp since it uses epoll, a Linux API for handling file descriptors efficiently, which has probably no perfect substitute for windows. Maybe select can be used for this.

- Example for preprocessing:
```c++
 #ifdef __linux__
  // linux code
 #elif _WIN32
  // windows code
 #endif
 ```
