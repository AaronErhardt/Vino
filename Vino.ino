void setup() {
  Serial.begin(9800);

  for (int x = 0; x <= 7; x++) {
    pinMode(x, OUTPUT);
  }

  Serial.print("Pins are configured now!");
}

void loop() {
  Serial.print("\nLights on!");

  for (int x = 0; x <= 7; x++) {
    digitalWrite(x, HIGH);
    delay(300);
  }
  delay(1000);

  Serial.print("\nLights off!");

  for (int x = 0; x <= 7; x++) {
    digitalWrite(x, LOW);
    delay(300);
  }
  delay(1000);
}
