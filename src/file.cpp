/* file.cpp
 *
 * Copyright (c) 2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#if defined(__linux__) || defined(_WIN32)

#include "file.hpp"

#include <dirent.h>

#include <fstream>
#include <iostream>
#include <vector>

#include "input.hpp"
#include "language.hpp"

conf::conf() {
  fstream conf_file;
  conf_file.open("etc/vino.config", fstream::in);

  if (!conf_file.is_open()) {
    cout << E_FILE_OPEN " (etc/vino.config)\n" << endl;
    abort();
  }

  string line, property;

  while (getline(conf_file, line)) {
    uint16_t length = line.length(), i = 0;
    for (; i < length; i++) {
      if (line.at(i) == '=') {
        property = line.substr(0, i);
        break;
      }
    }

    i++;

    if (property == "sketchbook") {
      sketchbook = line.substr(i, length - i);
    } else if (property == "language") {
      language = line.substr(i, length - i);
    } else if (property == "project") {
      project = line.substr(i, length - i);
    } else if (property == "editor") {
      editor = line.substr(i, length - i);
    } else {
      cout << L_BROKEN_CONFIG "\n\r";
      break;
    }
  }

  conf_file.close();
}

conf::~conf() {
  fstream conf_file;
  conf_file.open("etc/vino.config", fstream::out | fstream::trunc);

  if (!conf_file.is_open()) {
    cout << E_FILE_OPEN " (etc/vino.config)\n" << endl;
    abort();
  }

  conf_file << "sketchbook=" << sketchbook << endl;
  conf_file << "language=" << language << endl;
  conf_file << "project=" << project << endl;
  conf_file << "editor=" << editor << endl;

  conf_file.close();
}

proj::proj(conf *config)
    : path(config->sketchbook + config->project + "/" + config->project +
           ".ino") {}

proj *proj::open(conf *config, bool select) {
START:
  if (select) {
    cout << L_SELECT_PROJECT "\n\r";

    vector<string> projects;

    DIR *dir = opendir(config->sketchbook.c_str());

    if (dir == NULL) {
      cout << E_DIR_OPEN " (" << config->sketchbook << ")\n" << endl;
      abort();
    }

    struct dirent *entry = readdir(dir);

    int16_t projNum = 0;

    while (entry != NULL) {
      if (entry->d_type == DT_DIR && entry->d_name[0] != '.') {
        projNum++;
        cout << '[' << projNum << "] " << entry->d_name << "\n\r";
        cout.flush();
        string str_cast(entry->d_name);

        projects.push_back(str_cast);
      }

      entry = readdir(dir);
    }

    closedir(dir);

    char in;
    uint16_t selectedNum = 0;

    for (int16_t i = projNum; i > 0; i = (i - i % 10) / 10) {
      in = getChar(-1);

      if (in < '0' || in > '9') break;

      cout << in;
      cout.flush();
      selectedNum = selectedNum * 10 + (in - '0');
    }

    cout << "\n\r";

    if (selectedNum == 0 || selectedNum > projNum) {
      cout << E_NUM_CANT_BE_SELECTED << " [" << selectedNum << "]"
           << "\n\r";
      goto START;  // using goto because recursion in a constructor is weird
    }
    cout << L_SELECTED_PROJECT << projects[selectedNum - 1] << "\n\r";

    config->project = projects[selectedNum - 1];
  }

  fstream conf_file;
  string path =
      config->sketchbook + config->project + "/" + config->project + ".ino";
  conf_file.open(path, fstream::in);

  if (!conf_file.is_open()) {
    cout << E_FILE_OPEN ": " << path << "\n\r" << endl;
    select = true;
    goto START;
  }

  conf_file.close();

  return new proj(config);
}

#endif
