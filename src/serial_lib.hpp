/* serial_lib.hpp: Defines the serial class for the simulation
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __STD_HEADER__
#define __STD_HEADER__
#include <iostream>
using namespace std;
#endif

class __serial__ {
 public:
  string buf = "TEST\n";
  void begin(long);
  size_t print(string);
  size_t print(int);
  size_t println(string);
  size_t println(int);
};
