/* error.cpp
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include "error.hpp"

#include <termios.h>
#include <unistd.h>

#include <iomanip>

#include "colors.hpp"
#include "console_util.hpp"
#include "language.hpp"

void handleError(string current_command, string issue, string example,
                 uint8_t error_level) {
  currentCommand(current_command);
  resetBox();
  moveCursor(30, 1);
  cout << C_BLD C_ORG "-" L_ISSUE C_RST << issue << moveEndl() << moveRight(1);
  cout << C_TEST C_BLD "-" L_EXAMPLE C_RST << example << C_RST << moveEndl();

  logAdd(0, current_command + " => " + issue + " [" + to_string(error_level) +
                "]");

  switch (error_level) {
    case 0:
#ifdef HANDLE_CRIT_ERRORS

      cout << moveRight(1) << C_BLD C_RED "-" E_CRITICAL C_RST E_CRITICAL_EXPL
           << moveEndl();
      cout << moveRight(1) << C_BLD C_BLU "-" I C_RST I_EXPL << moveEndl();
      moveCursor(LAST_ROW, 0);
      cout.flush();
      abort();
#else
      serialAdd("ERROR: " + current_command + " => " + issue + " [" +
                to_string(error_level) + "]");
#endif
      break;
    case 1:
#ifdef SHOW_ERRORS

      cout << moveRight(1) << C_BLD C_BR_YLW "-" E C_RST E_EXPL << moveEndl();
      cout << moveRight(1) << C_BLD C_BLU "-" I C_RST I_EXPL << moveEndl();
      cout.flush();
      animateStatus(L_CURRENT_COMMAND + current_command, BC_BR_YLW C_BLD C_BLC,
                    3200000);
      resetBox();
#else
      serialAdd("ERROR: " + current_command + " => " + issue + " [" +
                to_string(error_level) + "]");
#endif
      break;
    case 2:
#ifdef SHOW_WARNINGS

      cout << moveRight(1) << C_BLD C_BR_CYN "-" L_WARNING C_RST E_EXPL
           << moveEndl();
      cout << moveRight(1) << C_BLD C_BLU "-" I C_RST I_EXPL << moveEndl();
      cout.flush();
      animateStatus(L_CURRENT_COMMAND + current_command, BC_BR_CYN C_BLD C_BLC,
                    3200000);
      resetBox();
#else
      serialAdd("ERROR: " + current_command + " => " + issue + " [" +
                to_string(error_level) + "]");
#endif
      break;
  }
}

#endif
