/* setup_console.hpp: Sets up the UI, signals, files, input etc.
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

void setupConsole();
/*  Gets executed once at the beginning
 1. Sets up signal handling for SIGINT and SIGABRT so the program can exit
    smooth. It also prints all log files before the program exits, the log
    files may not be available while the program is still runnning
 2. Sets up "raw" terminal mode (more information: README-dev)
 3. Calls setupUI and setupInput from console_util.hpp and input.hpp*/
