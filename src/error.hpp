/* error.hpp
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* If SHOW_WARNINGS is not defined warnings will be skipped, but shown in a
 * single line of the Info-Box*/
#define SHOW_WARNINGS
/* If SHOW_ERRORS is not defined non-critical errors will be skipped, but shown
 * in a single line of the Info-Box*/
#define SHOW_ERRORS
/* If HANDLE_CRIT_ERRORS is not defined critical errors will be skipped, but
 * shown in a single line of the Info-Box*/
#define HANDLE_CRIT_ERRORS

#ifndef __STD_HEADER__
#define __STD_HEADER__
#include <iostream>
using namespace std;
#endif

void handleError(string, string, string, uint8_t);
/* Handles errors showing some info in the box
 > PARAMETER 0: Specifies the currently executed command
                (via the currentCommand function)
 > PARAMETER 1: Describes the issue
 > PARAMETER 2: Example for a correct solution for the issue
 > PARAMETER 3: Specifies the level of the issue:
                 - 0 critical error (program will stop)
                 - 1 error (program will continue)
                 - 2 warning (program will continue)          */
