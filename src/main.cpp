/* main.cpp: Main loop for Arduino sketches
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include <unistd.h>

#include <iostream>

#include "arduino.hpp"
#include "language.hpp"
#include "setup_console.hpp"
using namespace std;

int main() {
  // The heading escape sequence may not be supported by all terminal emulators
  // The second part of the command hides the cursor (\e[?25l)
  cout << "\e]0;" L_HEADING "\007\e[?25l";

  // Further explained in setup_console.hpp
  setupConsole();

  // The Arduino section: calling setup() once and loop(), well, in a loop
  setup();
  for (;;) {
    loop();
    usleep(1);  // reduces CPU usage in idle mode massively while having almost
                // no effect on the program
  }
  cout << "\r\e[38;2;255;0;0m" W_UNEXPECTED_EXIT "\e[0m";
  abort();
  return 0;
}

#endif
