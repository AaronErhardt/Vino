/* sim_lib.hpp
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

// Arduino specific commands, further infos can be found in the Arduino
// documentation
void pinMode(uint8_t, uint8_t);

int digitalRead(uint8_t);

void digitalWrite(uint8_t, uint8_t);

void analogWrite(uint8_t, int);

int analogRead(uint8_t);

unsigned long millis(void);

unsigned long micros(void);

void delay(unsigned long);

void delayMicroseconds(unsigned int);

void randomSeed(unsigned long seed);

long random(long);

long random(long, long);

long map(long, long, long, long, long);
