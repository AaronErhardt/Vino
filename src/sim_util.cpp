/* sim_util.cpp
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include "sim_util.hpp"

#include <unistd.h>

#include <iomanip>

#include "arduino_constants.hpp"
#include "colors.hpp"
#include "console_util.hpp"
#include "error.hpp"
#include "input.hpp"
#include "language.hpp"

__pin__::__pin__(uint8_t pin_type, uint8_t pin_io, string pin_num,
                 string pin_name)
    : type(pin_type), io(pin_io), num(pin_num), name(pin_name) {}

uint8_t __pin__::update(string) { return 0; }
void __pin__::display() {}

__pin__::~__pin__() {}

__switch__::__switch__(uint8_t pin_io, string pin_num, string pin_name)
    : __pin__(switch_t, pin_io, pin_num, pin_name) {}

__potentiometer__::__potentiometer__(uint8_t pin_io, string pin_num,
                                     string pin_name)
    : __pin__(poti_t, pin_io, pin_num, pin_name) {}

__LED__::__LED__(uint8_t pin_io, string pin_num, string pin_name)
    : __pin__(LED_t, pin_io, pin_num, pin_name) {
  color = (pin_num.at(1) - '0') % 8;
}

void __switch__::display() {
  cout << displayVoltage() << "V" C_RST "  " C_BR_CYN << num;

  if (active == true) {
    cout << C_RED "├───";
  } else {
    cout << C_BLU "├──┐" C_RED;
  }
  cout << "──┤" C_RST "   " C_BR_CYN L_SWITCH " " << name;

  cout.flush();
}

void __potentiometer__::display() {
  cout << displayVoltage() << "V" C_RST "  " C_BR_CYN << num << wireColor()
       << "├───" C_RST "[" C_RED;

  if (resistance < 10) {
    cout << "00";
  } else if (resistance < 100) {
    cout << "0";
  }
  cout << to_string(resistance) << C_RST "|" C_BLU;

  if (500 - resistance < 10) {
    cout << "00";
  } else if (500 - resistance < 100) {
    cout << "0";
  }
  cout << to_string(500 - resistance)
       << C_RST "]   " C_BR_CYN L_POTENTIOMETER " " << name;

  cout.flush();
}

void __LED__::display() {
  cout << C_BR_CYN L_LED " " << name;
  if (io == P)
    cout << " ~ ";
  else
    cout << "   ";

  cout << C_BLU "├──";

  unsigned short int rgb_value = 50 + min(200, (voltage - voltage % 5) / 5);
  switch (color) {
    case 0:
      cout << "\e[38;2;" + to_string(rgb_value) + ";" + to_string(rgb_value) +
                  ";" + to_string(rgb_value) + "m■";
      break;
    case 1:
      cout << "\e[38;2;" + to_string(rgb_value) + ";50;50m■";
      break;
    case 2:
      cout << "\e[38;2;50;" + to_string(rgb_value) + ";50m■";
      break;
    case 3:
      cout << "\e[38;2;50;50;" + to_string(rgb_value) + "m■";
      break;
    case 4:
      cout << "\e[38;2;" + to_string(rgb_value) + ";" + to_string(rgb_value) +
                  ";50m■";
      break;
    case 5:
      cout << "\e[38;2;" + to_string((rgb_value - rgb_value % 4) / 4 * 3 + 13) +
                  ";50;" + to_string(rgb_value) + "m■";
      break;
    case 6:
      cout << "\e[38;2;50;" + to_string(rgb_value) + ";" +
                  to_string(rgb_value) + "m■";
      break;
    case 7:
      cout << "\e[38;2;" + to_string(rgb_value) + ";" +
                  to_string((rgb_value - rgb_value % 2) / 2 + 25) + ";50m■";
      break;
  }
  cout << wireColor() << "──" C_RST "[R]" << wireColor() << "──┤" C_BR_CYN
       << num << "  " << displayVoltage() + "V" C_RST;

  cout.flush();
}

string __pin__::displayVoltage() {
  string ret = to_string((float)voltage / 1023 * 5).substr(0, 4);

  switch (mode) {
    case 0:
      return C_BR_YLW + ret;
    case 1:
      return C_ORG + ret;
    case 2:
      return C_GRN + ret;
  }
  return C_GRY + ret;
}

string __pin__::wireColor() {
  return "\e[38;2;" + to_string(min(200, (voltage - voltage % 5) / 5)) + ";" +
         to_string(50 - min(50, (voltage - voltage % 25) / 25)) + ";" +
         to_string(255 - min(255, (voltage - voltage % 4) / 4)) + "m";
}

uint8_t __switch__::update(string current_command) {
  resetBox();
  moveCursor(30, 2);
  cout << C_BR_CYN L_INPUT_REQUIRED ": " L_SWITCH " " + name + C_RST
       << moveEndl();
  cout << moveRight(2) << "[1, " << L_SHORT_YES
       << "] -> " L_ON " " C_RED "+5V" C_RST << moveEndl();
  cout << moveRight(2) << "[2, " << L_SHORT_NO
       << "] -> " L_OFF " " C_BLU "GND" C_RST << moveEndl();
  cout << moveRight(2) << "[?,  ] -> " L_NO_CHANGES << moveEndl();
  cout.flush();

  char input;
  moveCursor(28, 1);
  cout << BC_BR_CYN C_BLC C_BLD;
  unsigned short int length = current_command.length();

  for (uint8_t x = 0; x < 81; x++) {
    input = getChar(20);
    if (input == '\0') {
      if (x < length) {
        cout << current_command.at(x);
      } else {
        cout << " ";
      }
      cout.flush();
    } else {
      break;
    }
  }
  cout << C_RST;
  switch (input) {
    case '1':
    case 'y':
      active = true;
      voltage = 1023;
      moveCursor(31, 2);
      cout << C_BLD " [1, " << L_SHORT_YES << "] -> " L_ON " " C_RED "+5V" C_RST
           << moveEndl();
      break;
    case '2':
    case 'n':
      active = false;
      voltage = 0;
      moveCursor(32, 2);
      cout << C_BLD " [2, " << L_SHORT_NO << "] -> " L_OFF " " C_BLU "GND" C_RST
           << moveEndl();
      break;
    default:
      moveCursor(33, 2);
      cout << C_BLD " [?,  ] -> " L_NO_CHANGES << moveEndl();
      break;
  }
  usleep(100000);
  return 0;
}

uint8_t __potentiometer__::update(string current_command) {
  resetBox();
  moveCursor(30, 2);
  cout << C_BR_CYN L_INPUT_REQUIRED ": " L_POTENTIOMETER "  " + name +
              " (0 - 500)" C_RST
       << moveEndl();
  cout << moveRight(2) << "[0- 9] -> " L_ENTER_NEW_VALUE << moveEndl();
  cout << moveRight(2) << "[?,  ] -> " L_NO_CHANGES << moveEndl();
  cout << moveRight(2)
       << L_RESULT ": [" C_RED "???" C_RST "|" C_BLU "???" C_RST "]"
       << moveEndl();
  cout.flush();

  char input[3] = {'?', '?', '?'};
  uint8_t input_num = 0;
  moveCursor(28, 1);
  cout << BC_BR_CYN C_BLC C_BLD;
  unsigned short int length = current_command.length();
  short int new_resistance = -1;
  uint8_t input_timeout = 40;

  for (uint8_t x = 0; x < 81 && input_num < 3; x++) {
    input[input_num] = getChar(input_timeout);

    if (input[input_num] == '\0') {
      if (x < length) {
        cout << current_command.at(x);
      } else {
        cout << " ";
      }
      cout.flush();
    } else if (isNumber(input[input_num])) {
      input_timeout = 160;

      moveCursor(33, sizeof(L_RESULT) + 4 + input_num);
      cout << C_RST C_RED << input[input_num];
      input_num++;

      moveCursor(28, x + 1);
      cout << BC_BR_CYN C_BLC C_BLD;
      if (x < length) {
        cout << current_command.at(x);
      } else {
        cout << " ";
      }
    } else {
      break;
    }
  }
  if (input_num == 3) {
    new_resistance = (int(input[0]) - 48) * 100 + (int(input[1]) - 48) * 10 +
                     (int(input[2]) - 48);
  }
  cout << C_RST;

  if (new_resistance < 0 || 500 < new_resistance) {
    moveCursor(32, 2);
    cout << C_BLD " [?,  ] -> " L_NO_CHANGES << moveEndl();
    if (new_resistance == -1) {
      for (uint8_t x = 0; x < 3; x++) {
        if (input[x] == '\0') {
          input[x] = '?';
        }
      }
      cout << moveRight(2) << " " L_RESULT ": [" C_RED << input[0] << input[1]
           << input[2]
           << C_RST C_BLD "|" C_BLU "???" C_RST C_BLD "] -> " L_NO_VALID_INPUT
           << moveEndl();
    } else {
      cout << moveRight(2) << " " L_RESULT ": [" C_RED << resistance
           << C_RST C_BLD "|" C_BLU "???" C_RST C_BLD "] -> " L_OUT_OF_RANGE
                          "! " C_RST "(>500)"
           << moveEndl();
    }
    cout.flush();
  } else {
    moveCursor(31, 2);
    resistance = new_resistance;
    voltage = 1023 - int(1023 * (double)resistance / 500);
    cout << C_BLD " [0- 9] -> " L_ENTER_NEW_VALUE << moveEndl() << moveEndl();
    cout << moveRight(2) << " " L_RESULT ": [" C_BLD C_RED << "tes"
         << C_RST C_BLD "|" C_BLD C_BLU << "est" << C_RST C_BLD "]"
         << moveEndl();
  }
  usleep(100000);
  return 0;
}

uint8_t __LED__::update(string command) { return -1; }

string Digital2String(uint8_t arg_0) {
  if (arg_0 == HIGH) {
    return "HIGH";
  } else if (arg_0 == LOW) {
    return "LOW";
  }
  return "?";
}

void __arduino__::display() {
  updateBox();
  // move to the top left-hand corner
  moveCursor(0, 0);
  // set precision for floating point numbers:
  cout << fixed << setprecision(2);
  // 1st line:
  cout << L_LED_SPACING "      " C_BLU "GND" C_RST "         " C_BR_GRY
                        "┌─────────────────────────┐" C_RST "    " C_RED
                        "+5V" C_RST " " C_BLU "GND" C_RST
       << moveEndl();
  // 2nd line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST "   " C_BLU "│" C_RST
       << moveEndl();
  // 3rd line:
  cout << "  ";
  pins[0]->display();
  cout << "       ";
  pins[15]->display();
  cout << moveEndl();
  // 4th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "            " C_ARD C_BLD "A" C_RST
          "            " C_BR_GRY "│" C_RST "     " C_RED "│" C_RST "   " C_BLU
          "│" C_RST
       << moveEndl();
  // 5th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST "   " C_BLU "│" C_RST
       << moveEndl();
  // 6th line:
  cout << "  ";
  pins[1]->display();
  cout << "       ";
  pins[14]->display();
  cout << moveEndl();
  // 7th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "            " C_ARD C_BLD "R" C_RST
          "            " C_BR_GRY "│" C_RST "     " C_RED "│" C_RST "   " C_BLU
          "│" C_RST
       << moveEndl();
  // 8th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST "   " C_BLU "│" C_RST
       << moveEndl();
  // 9th line:
  cout << "  ";
  pins[2]->display();
  cout << "       ";
  pins[13]->display();
  cout << moveEndl();
  // 10th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "            " C_ARD C_BLD "D" C_RST
          "            " C_BR_GRY "│" C_RST "     " C_RED "│" C_RST
       << moveEndl();
  // 11th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST
       << moveEndl();
  // 12th line:
  cout << "  ";
  pins[3]->display();
  cout << "       ";
  pins[12]->display();
  cout << moveEndl();
  // 13th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "            " C_ARD C_BLD "U" C_RST
          "            " C_BR_GRY "│" C_RST "  " C_BLU "┴" C_RST "  " C_RED
          "│" C_RST
       << moveEndl();
  // 14th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST
       << moveEndl();
  // 15th line:
  cout << "  ";
  pins[4]->display();
  cout << "       ";
  pins[11]->display();
  cout << moveEndl();
  // 16th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "            " C_ARD C_BLD "I" C_RST
          "            " C_BR_GRY "│" C_RST "  " C_BLU "┴" C_RST "  " C_RED
          "│" C_RST
       << moveEndl();
  // 17th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST
       << moveEndl();
  // 18th line:
  cout << "  ";
  pins[5]->display();
  cout << "       ";
  pins[10]->display();
  cout << moveEndl();
  // 19th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "            " C_ARD C_BLD "N" C_RST
          "            " C_BR_GRY "│" C_RST "  " C_BLU "┴" C_RST "  " C_RED
          "│" C_RST
       << moveEndl();
  // 20th line:s:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST
       << moveEndl();
  // 21th line:
  cout << "  ";
  pins[6]->display();
  cout << "       ";
  pins[9]->display();
  cout << moveEndl();
  // 22th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "            " C_ARD C_BLD "O" C_RST
          "            " C_BR_GRY "│" C_RST "  " C_BLU "┴" C_RST "  " C_RED
          "│" C_RST
       << moveEndl();
  // 23th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "     " C_RED "│" C_RST
       << moveEndl();
  // 24th line:
  cout << "  ";
  pins[7]->display();
  cout << "       ";
  pins[8]->display();
  cout << moveEndl();
  // 25th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST
       << "          " C_BR_GRY "│" C_RST "                         " C_BR_GRY
          "│" C_RST "  " C_BLU "┴" C_RST "  " C_RED "│" C_RST
       << moveEndl();
  // 26th line:
  cout << L_LED_SPACING "       " C_BLU "│" C_RST "          " C_BR_GRY
                        "└─────────────────────────┘" C_RST "     " C_RED
                        "│" C_RST
       << moveEndl();
  // 27th line:
  cout << L_LED_SPACING "      " C_BLU "GND" C_RST
                        "                                        " C_RED
                        "+5V" C_RST
       << moveEndl();
  cout.flush();
}

#endif
