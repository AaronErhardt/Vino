/* benchmark.cpp
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include <unistd.h>

#include <chrono>
#include <iomanip>
#include <iostream>

#include "arduino.hpp"
#include "colors.hpp"
#include "console_util.hpp"
#include "language.hpp"
#include "setup_console.hpp"
#include "sim_lib.hpp"
#include "sim_util.hpp"
using namespace std;
using namespace std::chrono;

#define EXEC_NUM 1000000

int main() {
  // The heading escape sequence may not be supported by all terminal emulators
  // The second part of the command hides the cursor (\e[?25l)
  cout << "\e]0;Benchmark\007\e[?25l";

  // Further explained in setup_console.hpp
  setupConsole();

  // benchmark -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

  moveCursor(30, 2);
  cout << "Started benchmark!";
  cout.flush();

  // preparation code
  moveCursor(0, 0);
  string test = "\e[20C\r";

  high_resolution_clock::time_point time_start =
      high_resolution_clock::now();  // save the time at the start

  for (unsigned long t = 0; t < EXEC_NUM; t++) {
    // test funtions here
    // updateArduino();
    // currentCommand("BENCHMARK");
    cout << test;
  }

  high_resolution_clock::time_point time_end =
      high_resolution_clock::now();  // save the time at the end

  long duration =
      long(duration_cast<microseconds>(time_end - time_start)
               .count());  // calculating the time spent for the test function
  // cout << "\n\n\n\n\e[4A\e[2C\r";
  moveCursor(30, 2);
  cout << fixed << setprecision(17);
  cout << "Executed the code " << EXEC_NUM << " times ("
       << (double)duration / EXEC_NUM << " micros per execution)" << moveEndl();
  cout << moveRight(2) << "Time in micros: " << duration << moveEndl();
  cout << moveRight(2) << "Time in millis: " << double(duration) / 1000
       << moveEndl();
  cout << moveRight(2) << "Time in seconds: " << double(duration) / 1000000;
  cout.flush();
  abort();
  return 0;
}

#endif
