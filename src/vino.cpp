/* vino.cpp
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#define VINO_VERSION "BETA: 0.3.1"

#if defined(__linux__) || defined(_WIN32)

#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include <iostream>
#include <vector>

#include "colors.hpp"
#include "file.hpp"
#include "input.hpp"
#include "language.hpp"
using namespace std;

termios old_terminal, new_terminal;

conf *config;
proj *project;

void intHandler(int signal) {
  delete config;
  closeEpollFd();
  tcsetattr(STDIN_FILENO, TCSANOW, &old_terminal);
  cout << C_RST "\e[?25h\n\r"
       << L_PROGRAM_STOPPED " (" C_BR_CYN L_CTR_C C_RST ")" << endl;
  exit(0);
}

void abrtHandler(int signal) {
  delete config;
  closeEpollFd();
  tcsetattr(STDIN_FILENO, TCSANOW, &old_terminal);
  cout << C_RST "\e[?25h\r" << L_VINO_BYE << endl;
  exit(0);
}

struct sigaction sig_int_handler, sig_abrt_handler;

void setupSignals() {
  sig_int_handler.sa_handler = intHandler;
  sig_abrt_handler.sa_handler = abrtHandler;
  sigemptyset(&sig_int_handler.sa_mask);
  sigemptyset(&sig_abrt_handler.sa_mask);
  sig_int_handler.sa_flags = 0;
  sig_abrt_handler.sa_flags = 0;

  sigaction(SIGINT, &sig_int_handler, NULL);
  sigaction(SIGABRT, &sig_abrt_handler, NULL);

  tcgetattr(STDIN_FILENO, &old_terminal);
  new_terminal = old_terminal;
  new_terminal.c_iflag &=
      ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
  new_terminal.c_oflag &= ~OPOST;
  new_terminal.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN);
  new_terminal.c_cflag &= ~(CSIZE | PARENB);
  new_terminal.c_cflag |= CS8;
  tcsetattr(STDIN_FILENO, TCSANOW, &new_terminal);
}

const string commands[] = {C_AUTORUN, C_CLEAN,  C_COMPILE, C_EDIT, C_ERRORS,
                           C_EXIT,    C_HELP,   C_LIST,    C_QUIT, C_SERIAL,
                           C_RUN,     C_UPDATE, C_VERSION, C_OPEN};

void autoComplete(string *input) {
  uint8_t length = input->length();
  if (length == 0) {
    uint8_t length = sizeof(commands) / sizeof(commands[0]);
    cout << C_BR_GRY "\r" << endl;
    for (int8_t i = 0; i < length; i++) {
      cout << "  " + commands[i] << '\r' << endl;
    }
    cout << C_RST "\r> ";
    return;
  }
  int8_t firstResult = -1;

  for (int8_t i = sizeof(commands) / sizeof(commands[0]) - 1; i >= 0; i--) {
    if (length <= commands[i].length()) {
      if (*input == commands[i].substr(0, length)) {
        if (firstResult == -1) {
          firstResult = i;
        } else if (firstResult == -2) {
          cout << "\r  " C_BR_GRY << commands[i] << C_RST << endl;
        } else {
          cout << "\r  " C_BR_GRY << commands[firstResult] << C_RST << endl;
          cout << "\r  " C_BR_GRY << commands[i] << C_RST << endl;
          firstResult = -2;
        }
      }
    }
  }
  if (firstResult >= 0) {
    *input = commands[firstResult];
  }
  cout << "\r> " << *input;
}

void copyCode() {
  string command =
      "echo '#if defined(__linux__) || defined(_WIN32)\n#include "
      "\"simulation_lib.hpp\"\n' > src/arduino.cpp && cat ";
  command += project->path.c_str();
  command += " >> src/arduino.cpp && echo '\n#endif' >> src/arduino.cpp";
  system(command.c_str());
}

void exec(string command) {
  if (command == C_OPEN) {
    project = proj::open(config);
    goto END;
  }

  tcsetattr(STDIN_FILENO, TCSANOW, &old_terminal);
  if (command == C_HELP) {
    cout << L_VINO_HELP << endl;

  } else if (command == C_COMPILE) {
    copyCode();
    system("cd src/ && make program");

  } else if (command == C_RUN) {
    system("cd src/ && ./program");

  } else if (command == C_AUTORUN) {
    copyCode();
    system("cd src/ && make program && ./program");

  } else if (command == C_EDIT) {
    cout << L_NOTE_EDIT_MANUALLY << endl;
    string command = config->editor + " " + project->path;
    system(command.c_str());

  } else if (command == C_CLEAN) {
    system("cd src/ && make clean");

  } else if (command == C_UPDATE) {
    system("bash etc/update.sh");

  } else if (command == C_ERRORS) {
    system("cat log/error.log");

  } else if (command == C_SERIAL) {
    system("cat log/serial.log");

  } else if (command == C_EXIT || command == C_QUIT) {
    raise(SIGABRT);

  } else if (command == C_LIST) {
    uint8_t length = sizeof(commands) / sizeof(commands[0]);
    for (int8_t i = 0; i < length; i++) {
      cout << commands[i] << '\r' << endl;
    }

  } else if (command == C_VERSION) {
    cout << VINO_VERSION << endl;

  } else {
    cout << W_UNKNOWN_COMMAND << endl;
  }

  tcsetattr(STDIN_FILENO, TCSANOW, &new_terminal);
END:
  cout << "\r> ";
  cout.flush();
}

int main() {
  cout << L_VINO_WELCOME << endl;

  setupSignals();
  setupInput();

  config = new conf();
  project = proj::open(config, false);

  string input = "";
  static vector<string> history;
  history.push_back(input);
  static uint16_t hist_num = 0;

  cout << "\r> ";
  cout.flush();

  for (char in;;) {
    in = getChar(-1);

    if (in + 0 == 13) {
      if (input != "") {
        cout << "\r# " << input << "\r" << endl;
        if (history[history.size() - 2] != input) {
          history.back() = input;
          history.push_back("");
        }
        exec(input);
        hist_num = history.size() - 1;
        input = "";
      }
      continue;
    } else if (in == '\t') {
      autoComplete(&input);
    } else if (in + 0 == 127) {
      input = input.substr(0, input.length() - 1);
      cout << "\r> " << input << "  \e[2D";
    } else if (in + 0 >= 97 && in + 0 <= 122) {
      input += in;
      cout << "\r> " << input;
    } else if (in == '2') {
      if (hist_num == history.size() - 1) {
        history.back() = input;
      }
      if (hist_num > 0) {
        hist_num--;
      }
      input = history[hist_num];

      cout << "\r> " << input << "\e[K";
    } else if (in == '1') {
      if (hist_num == history.size() - 1) {
        input = history.back();
      }
      if (hist_num < history.size() - 1) {
        hist_num++;
      }
      input = history[hist_num];

      cout << "\r> " << input << "\e[K";
    }
    cout.flush();
  }
}

#endif
