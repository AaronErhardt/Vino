/* console_util.hpp: provides many useful functions for the console and logging
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __STD_HEADER__
#define __STD_HEADER__
#include <iostream>
using namespace std;
#endif

//#define UP "\e[1A"
//#define DOWN "\e[1B"
//#define RIGHT "\e[1C"
//#define LEFT "\e[1D"
//#define ERASE "\e[K"
#define LAST_ROW 34  // 0-34 -> 35 rows needed

//#define UPPER_LEFT 0
//#define STATUS_BAR 1

struct box_buffer {
  string buf[4] = {"\0", "\0", "\0", "\0"};
  int line[4] = {-1, -1, -1, -1};
  unsigned short int t_line = 0;
};

void serialAdd(string);
void logAdd(uint8_t, string);
/* Adds a string to the a log buffer
 > PARAMETER 0: Specifies which log will be selected:
                 - 0: error.log - stores errors
                 - 1: serial.log -timeStampAt stores serial communication
                 - 2: actions.log - stores any user input (not working yet)
 > PARAMETER 1: The string that will be added at the end of the buffer */

void openLogFiles();
void closeLogFiles();

string to_one_line(string);

void animateStatus(string, string, unsigned long int);
/* Shows a progress bar in the statur field
 > PARAMETER 0: Specifies the text that will be shown in the status bar
 > PARAMETER 1: Specifies the color of the text printed by the animation
 > PARAMETER 2: Specifies the duration ot the animation in microseconds */

void setupUI();
/* Sets up the box and the status bar made with ASCII character */

void moveCursor(uint8_t, uint8_t);
/* Moves the Cursor to a certain position in the console counting from the top
 * left-hand corner
 > PARAMETER 0: Number of rows
 > PARAMETER 1: Number of columns
 ! Useful PARAMETERS: moveCursor(28, 1) > beginning of the status bar
                      moveCursor(30, 1) > beginning of the box
                      moveCursor(LAST_ROW, 0) > beginning of the last row */
void resetStatus();
/* Fills the status bar with empty spaces */

void resetBox();
/* Fills the box with empty spaces */

void updateBox();

void currentCommand(string);
/* Displays the current command in the status bar
 > PARAMETER 0: Only the command! (like "pinMode(2, OUTPUT);" */

string moveEndl();
/* endl would move the curser without beeing tracked which would make precise
 * cursor movement (via moveCursor) impossible. moveEndl() works just like endl
 * but also stores changes to the cursor */

string moveUp(uint8_t);
string moveDown(uint8_t);
string moveRight(uint8_t);
string moveLeft(uint8_t);
/* move the curser in a cartain direction
 > PARAMETER 0: Speciifies the number of row/columns the cursor should move*/

bool isNumber(char);
/* returns true is the char is a number (0-9) otherwise false */
