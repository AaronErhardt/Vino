/* setup_console.cpp: Sets up the UI, signals, files, input etc.
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include "setup_console.hpp"

#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#include <iostream>

#include "clock.hpp"
#include "colors.hpp"
#include "console_util.hpp"
#include "input.hpp"
#include "language.hpp"
#include "sim_util.hpp"

termios old_terminal;

void prepare_exit() {
  moveCursor(LAST_ROW, 0);
  closeLogFiles();
  closeEpollFd();
  tcsetattr(STDIN_FILENO, TCSANOW, &old_terminal);
}

void intHandler(int signal) {
  prepare_exit();
  cout << C_RST "\e[?25h\n\r"
       << L_PROGRAM_STOPPED " (" C_BR_CYN L_CTR_C C_RST ")" << endl;
  exit(0);
}

void abrtHandler(int signal) {
  prepare_exit();
  cout << C_RST "\e[?25h" << endl;
  exit(0);
}

struct sigaction sig_int_handler, sig_abrt_handler;

void setupSignals() {
  sig_int_handler.sa_handler = intHandler;
  sig_abrt_handler.sa_handler = abrtHandler;
  sigemptyset(&sig_int_handler.sa_mask);
  sigemptyset(&sig_abrt_handler.sa_mask);
  sig_int_handler.sa_flags = 0;
  sig_abrt_handler.sa_flags = 0;

  sigaction(SIGINT, &sig_int_handler, NULL);
  sigaction(SIGABRT, &sig_abrt_handler, NULL);

  tcgetattr(STDIN_FILENO, &old_terminal);
  termios new_terminal = old_terminal;
  new_terminal.c_iflag &=
      ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
  new_terminal.c_oflag &= ~OPOST;
  new_terminal.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN);
  new_terminal.c_cflag &= ~(CSIZE | PARENB);
  new_terminal.c_cflag |= CS8;
  tcsetattr(STDIN_FILENO, TCSANOW, &new_terminal);
}

void setupConsole() {
// check for correct size of the terminal window
#ifdef __linux__
  struct winsize size;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
  if (size.ws_row < 35) {
    cout << L_SHOULD_SEE_FROM_BOTTOM "\n\r";
    for (int x = 2; x <= 33; x++) {
      cout << x << "\r\n";
    }
    cout << "34 \e[?25h" C_BLD L_RESIZE_ROWS C_RST << endl;
    exit(0);
  }
  if (size.ws_col < 83) {
    cout << L_FIT_IN_SINGLE_LINE << endl
         << "\r\e[?25h" C_BLD L_RESIZE_COLUMNS C_RST << endl;
    exit(0);
  }
#endif
  setupSignals();
  setupInput();
  openLogFiles();

  setupUI();
  // updateArduino();

  startClock();
}

#endif
