/* input.cpp
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include "input.hpp"

#include <sys/epoll.h>
#include <unistd.h>

#define BUFFER_SIZE 4

int event_count, i;
size_t bytes_read;
char input_buffer[BUFFER_SIZE + 1];
struct epoll_event event, events[1];
int epoll_fd = epoll_create1(0);

void setupInput() {
  if (epoll_fd == -1) {
    cout << "\rFailed to create epoll file descriptor\n";
    abort();
  }

  event.events = EPOLLIN;
  event.data.fd = 0;

  if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, 0, &event)) {
    cout << "\rFailed to add file descriptor to epoll\n";
    abort();
  }
}

void closeEpollFd() {
  if (close(epoll_fd)) {
    cout << "\rFailed to add file descriptor to epoll\n";
  }
}

char getChar(unsigned int timeout) {
  event_count = epoll_wait(epoll_fd, events, 1, timeout);
  if (event_count == 1) {
    bytes_read = read(events[0].data.fd, input_buffer, BUFFER_SIZE);
    return input_buffer[0];
  }
  return '\0';  // '\0' will be returned if no input was given
}

#endif
