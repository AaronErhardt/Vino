/* sim_util.hpp
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __STD_HEADER__
#define __STD_HEADER__
#include <iostream>
using namespace std;
#endif

#define D 0  // digital Pin
#define P 1  // PWM Pin
#define A 2  // analog Pin

#define LED_t 0
#define switch_t 1
#define poti_t 2

class __pin__ {
 public:
  unsigned short int voltage = 0;  // 0 - 1023 == 0V - 5V
  uint8_t mode = 0;                // 0 == INPUT, 1 == INPUT_PULLUP, 2 == OUTPUT
  uint8_t digital_status = 0;      // either HIGH or LOW, set to LOW by default
  uint8_t type;                    // LED, switch or potentiometer
  uint8_t io;                      // digital, PWM or analog
  string num;
  string name;
  bool updated = true;

  __pin__(uint8_t, uint8_t, string, string);
  ~__pin__();

  virtual uint8_t update(string);
  virtual void display();

  string wireColor();
  string displayVoltage();
};

class __switch__ : public __pin__ {
 public:
  bool active = false;
  uint8_t update(string);
  void display();
  __switch__(uint8_t, string, string);
};

class __potentiometer__ : public __pin__ {
 public:
  uint8_t update(string);
  void display();
  __potentiometer__(uint8_t, string, string);

  unsigned short int resistance = 500;
};

class __LED__ : public __pin__ {
 public:
  uint8_t update(string);
  void display();
  __LED__(uint8_t, string, string);

  uint8_t color;
};

/*
void setPinVoltage( uint8_t, unsigned short int);
void setPinMode( uint8_t,  uint8_t);
void setDigitalStatus( uint8_t,  uint8_t);
unsigned short int getPinVoltage( uint8_t);
 uint8_t getPinMode( uint8_t);
 uint8_t getDigitalStatus( uint8_t);
 * Accessing the variables defined in the __pin__ class
 * I'm not really happy using the objects a bit like simple structs, but the
 * variables need to be accessed from sim_lib.cpp where the objects are not
 * defined, therefore those functions avoid the problem for now */

string Pin2String(uint8_t);
/* Converts a pin number into a string */

string Digital2String(uint8_t);
/* Converts a pin number into a string */

/* Prints the whole Arduino block */

class __arduino__ {
 public:
  __pin__ *pins[16] = {
      new __LED__(D, "00", "0"),           new __LED__(D, "01", "1"),
      new __LED__(D, "02", "2"),           new __LED__(D, "03", "3"),
      new __LED__(D, "04", "4"),           new __LED__(P, "05", "5"),
      new __LED__(P, "06", "6"),           new __LED__(P, "07", "7"),
      new __switch__(D, "08", "0"),        new __switch__(D, "09", "1"),
      new __switch__(D, "10", "2"),        new __switch__(D, "11", "3"),
      new __switch__(D, "12", "4"),        new __potentiometer__(A, "A0", "0"),
      new __potentiometer__(A, "A1", "1"), new __potentiometer__(A, "A2", "2")};
  void display();
};
