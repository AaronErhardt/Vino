/* console_util.cpp: provides many useful functions for the console and logging
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#if defined(__linux__) || defined(_WIN32)

#include "console_util.hpp"

#include <unistd.h>

#include <fstream>

#include "clock.hpp"
#include "language.hpp"

uint8_t row_number = 0;     // current max is 25
uint8_t column_number = 0;  // current max is 82

string timeStamp();

string to_one_line(string str) {
  for (int x = str.size(); x >= 0; x--) {
    if (str[x] == '\n' || str[x] == '\r') {
      if (x == 0) {
        str = str.substr(1, str.length() - 1);
      } else {
        str[x] = ' ';
      }
    }
  }
  return str;
}

// 0 : error.log
// 1 : serial.log
// 2 : actions.log
fstream log_files[3];

void openLogFiles() {
  log_files[0].open("../log/error.log", fstream::out | fstream::trunc);
  log_files[1].open("../log/serial.log", fstream::out | fstream::trunc);
  log_files[2].open("../log/actions.log", fstream::out | fstream::trunc);

  if (!log_files[0].is_open()) {
    serialAdd(E_FILE_OPEN " (error.log)\n");
  }
  if (!log_files[1].is_open()) {
    serialAdd(E_FILE_OPEN " (serial.log)\n");
  }
  if (!log_files[2].is_open()) {
    serialAdd(E_FILE_OPEN " (action.log)\n");
  }
}

void closeLogFiles() {
  log_files[0].close();
  log_files[1].close();
  log_files[2].close();
}

void logAdd(uint8_t log_num, string str) {
  if (log_files[log_num].is_open()) {
    log_files[log_num] << timeStamp() << " " << str;
    if (str.back() != '\n') log_files[log_num] << "\n";

    log_files[log_num].flush();
  }
}

string timeStamp() {
  unsigned long temp = millisSinceStart();

  unsigned short int millis = temp % 1000;
  temp = (temp - millis) / 1000;
  uint8_t seconds = temp % 60;
  temp = (temp - seconds) / 60;
  uint8_t minutes = temp % 60;
  temp = (temp - minutes) / 60;
  uint8_t hours = temp % 100;

  return "[" + ((hours > 9) ? to_string(hours) : "0" + to_string(hours)) + ":" +
         ((minutes > 9) ? to_string(minutes) : "0" + to_string(minutes)) + ":" +
         ((seconds > 9) ? to_string(seconds) : "0" + to_string(seconds)) + ":" +
         ((millis > 99) ? to_string(millis)
                        : ((millis > 9) ? "0" + to_string(millis)
                                        : "00" + to_string(millis))) +
         "]";
}

struct box_buffer s_out;

// Maybe I should add an explanation for the code below...
void serialAdd(string str) {
  uint8_t a_line = 0;
  if (s_out.line[0] == -2) {
    str = s_out.buf[0] + str;
  } else {
    s_out.t_line++;
  }
  unsigned short int length = str.length();
  unsigned short int last_cut = 0;

  for (unsigned short int x = last_cut; x < length; x++) {
    if (str.at(x) == '\n') {
      if (a_line != 0) {
        s_out.line[0] = s_out.t_line;
        s_out.t_line++;
        for (int y = 3; y > 0; y--) {
          s_out.buf[y] = s_out.buf[y - 1];
          s_out.line[y] = s_out.line[y - 1];
        }
      }
      s_out.buf[0] = str.substr(last_cut, x - last_cut);
      s_out.line[0] = s_out.t_line;
      for (int y = 3; y > 0; y--) {
        s_out.buf[y] = s_out.buf[y - 1];
        s_out.line[y] = s_out.line[y - 1];
      }
      s_out.buf[0] = "";
      a_line = 0;
      s_out.t_line++;
      s_out.line[0] = -2;
      last_cut = x + 1;
      continue;
    }
    if (x - last_cut > 75) {
      if (a_line == 0) {
        s_out.buf[0] = str.substr(last_cut, x - last_cut + 1);
        s_out.line[0] = s_out.t_line;
        a_line++;
        last_cut = x + 1;
        continue;
      }
      s_out.line[0] = s_out.t_line;
      for (int y = 3; y > 0; y--) {
        s_out.buf[y] = s_out.buf[y - 1];
        s_out.line[y] = s_out.line[y - 1];
      }
      s_out.line[0] = -2;
      s_out.buf[0] = str.substr(last_cut, x - last_cut);

      a_line++;
      last_cut = x + 1;
    }
  }
  if (last_cut < length) {
    if (a_line != 0) {
      for (int y = 3; y > 0; y--) {
        s_out.buf[y] = s_out.buf[y - 1];
        s_out.line[y] = s_out.line[y - 1];
      }
    }
    s_out.buf[0] = str.substr(last_cut, length - last_cut);
    s_out.line[0] = -2;
  }
}

void updateBox() {
  resetBox();
  moveCursor(30, 1);
  short int last_num = 0;

  for (signed char x = 3; x >= 0; x--) {
    if (s_out.line[x] == -1 || s_out.line[x] == last_num ||
        s_out.t_line == last_num) {
      cout << "    ";
    } else if (s_out.line[x] == -2) {
      cout << ((s_out.t_line % 100 < 10) ? " " : "") +
                  to_string(s_out.t_line % 100) + ": ";
    } else {
      cout << ((s_out.line[x] % 100 < 10) ? " " : "") +
                  to_string(s_out.line[x] % 100) + ": ";
    }
    cout << s_out.buf[x] << moveEndl() << moveRight(1);
    last_num = s_out.line[x];
  }
}

void setupUI() {
  for (uint8_t x = 0; x < LAST_ROW; x++) {
    cout << "-"
         << "\e[K\n\r";
    // cout << (int)x << "\e[K\n\r";  // Use for debugging UI
    row_number = LAST_ROW;
  }

  cout << moveUp(7)
       << "╔═══════════════════════════════════════════════════════════════════"
          "══════════════╗"
       << moveEndl();
  cout << "║                                                                   "
          "              ║"
       << moveEndl();
  cout << "╠═══════════════════════════════════════════════════════════════════"
          "══════════════╣"
       << moveEndl();
  for (int x = 0; x < 4; x++) {
    cout << "║                                                                 "
            "                ║"
         << moveEndl();
  }
  cout << "╚═══════════════════════════════════════════════════════════════════"
          "══════════════╝"
       << "\r";
  cout.flush();
}

void moveCursor(uint8_t row, uint8_t column) {
  if (row > row_number) {
    cout << "\e[" << row - row_number << "B";
  } else if (row < row_number) {
    cout << "\e[" << row_number - row << "A";
  }

  row_number = row;
  if (column > 0) {
    cout << "\r\e[" << (int)column << "C";
  } else {
    cout << "\r";
  }
  column_number = column;
}

void resetStatus() {
  moveCursor(28, 1);
  cout << "\e[0m                                                             "
          "                    \r\e[1C";
}

void resetBox() {
  moveCursor(30, 0);
  for (int x = 0; x < 4; x++) {
    cout << "║                                                                 "
            "                ║"
         << moveEndl();
  }
}

string moveEndl() {
  row_number++;
  column_number = 0;
  return "\e[1B\r";
}

string moveUp(uint8_t arg_0) {
  row_number -= arg_0;
  return "\e[" + to_string(arg_0) + "A";
}

string moveDown(uint8_t arg_0) {
  row_number += arg_0;
  return "\e[" + to_string(arg_0) + "B";
}

string moveRight(uint8_t arg_0) {
  column_number += arg_0;
  return "\e[" + to_string(arg_0) + "C";
}

string moveLeft(uint8_t arg_0) {
  column_number -= arg_0;
  return "\e[" + to_string(arg_0) + "D";
}

bool isNumber(char ch) {
  if (ch > 47 && ch < 58) {
    return true;
  }
  return false;
}

void currentCommand(string command) {
  resetStatus();
  cout << L_CURRENT_COMMAND << command;
}

void animateStatus(string current_command, string color_code,
                   unsigned long int delay) {
  resetStatus();
  moveCursor(28, 1);
  unsigned short int length = current_command.length();

  if (delay > 800000) {
    cout << current_command << "\r\e[1C" << color_code;
    unsigned long int sleep_time = (delay - delay % 81) / 81;
    usleep((useconds_t)(delay % 81));

    for (uint8_t x = 0; x < 81; x++) {
      usleep((useconds_t)sleep_time);
      if (x < length) {
        cout << current_command.at(x);
      } else {
        cout << " ";
      }
      cout.flush();
    }
  } else if (delay > 200000) {  // printing 5 chars at once, the human eye
                                // doesn't notice the difference
    cout << current_command << "\r\e[1C" << color_code;
    unsigned long int sleep_time = (delay - delay % 16) / 16;
    usleep((useconds_t)(delay % 16));
    cout << current_command.at(0);

    for (uint8_t x = 0; x < 16; x++) {
      usleep((useconds_t)sleep_time);
      for (uint8_t y = x * 5 + 1; y < (x * 5 + 6); y++) {
        if (y < length) {
          cout << current_command.at(y);
        } else {
          cout << " ";
        }
        cout.flush();
      }
    }
  } else {  // no need to animate anything, the human eye couln't sea it anyway
    cout << current_command;
    usleep((useconds_t)delay);
    cout.flush();
  }
  cout << "\e[0m";
}

#endif
