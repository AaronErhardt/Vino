/* serial_lib.cpp: Defines the serial class for the simulation
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include "serial_lib.hpp"

#include "console_util.hpp"
#include "error.hpp"
#include "language.hpp"
#include "sim_util.hpp"

bool serial_com_active = false;

void check_com_active() {
  if (serial_com_active == false) {
    //
  }
}

void __serial__::begin(long bps) {
  if (bps < 0 || bps > 200000) {
    handleError("Serial.begin(" + to_string(bps) + ");",
                E_SERIALBEGIN_OUT_OF_RANGE, E_SERIALBEGIN_EXPL, 0);
  }

  serial_com_active = true;

  logAdd(1, L_SERIAL_ESTABLISHED + to_string(bps) + "bps\n");
  // serialAdd(L_SERIAL_ESTABLISHED + to_string(bps) + "bps\n");
  updateBox();
}

size_t __serial__::print(string input) {
  if (serial_com_active == false) {
    handleError("Serial.print(\"" + input + "\");", E_SERIAL_COM_NOT_ACTIVE,
                E_ESTABLISH_SERIAL_COM_EXPL, 0);
  }

  serialAdd(input);
  updateBox();

  logAdd(1, to_one_line(input));

  return input.size();
}

size_t __serial__::print(int input) {
  if (serial_com_active == false) {
    handleError("Serial.print(\"" + to_string(input) + "\");",
                E_SERIAL_COM_NOT_ACTIVE, E_ESTABLISH_SERIAL_COM_EXPL, 0);
  }

  string str = to_string(input);
  serialAdd(str);
  updateBox();

  logAdd(1, to_one_line(str));

  return str.size();
}

size_t __serial__::println(string input) {
  if (serial_com_active == false) {
    handleError("Serial.println(\"" + input + "\");", E_SERIAL_COM_NOT_ACTIVE,
                E_ESTABLISH_SERIAL_COM_EXPL, 0);
  }

  serialAdd(input + "\n");
  updateBox();

  logAdd(1, to_one_line(input));

  return input.size();
}

size_t __serial__::println(int input) {
  if (serial_com_active == false) {
    handleError("Serial.println(\"" + to_string(input) + "\");",
                E_SERIAL_COM_NOT_ACTIVE, E_ESTABLISH_SERIAL_COM_EXPL, 0);
  }

  string str = to_string(input);
  serialAdd(str + "\n");
  updateBox();

  logAdd(1, to_one_line(str));

  return str.size();
}

// Error handling for wrong arguments
/*void __serial__::begin() {
  handleError("Serial.begin()", E_SERIALBEGIN_MISSING_INT, E_SERIALBEGIN_EXPL,
              0);
}*/

#endif
