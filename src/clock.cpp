/* clock.cpp
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include <chrono>
using namespace std::chrono;

#include "clock.hpp"

high_resolution_clock::time_point start_time;

void startClock() {
  start_time = high_resolution_clock::now();  // stores the time at the start
}

unsigned long microsSinceStart() {
  high_resolution_clock::time_point current_time =
      high_resolution_clock::now();  // stores the current time
  return (unsigned long)duration_cast<microseconds>(current_time - start_time)
      .count();
}

unsigned long millisSinceStart() {
  high_resolution_clock::time_point current_time =
      high_resolution_clock::now();  // stores the current time
  return (unsigned long)duration_cast<milliseconds>(current_time - start_time)
      .count();
}

#endif
