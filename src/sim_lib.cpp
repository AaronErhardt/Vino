/* sim_lib.cpp
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#if defined(__linux__) || defined(_WIN32)

#include <iostream>

#include "clock.hpp"
#include "colors.hpp"
#include "console_util.hpp"
#include "error.hpp"
#include "input.hpp"
#include "language.hpp"
#include "sim_util.hpp"
using namespace std;

#include "arduino_constants.hpp"
#include "sim_lib.hpp"

__arduino__ arduino;

// UTILITY

string Pin2String(uint8_t pin_num) {
  int8_t arrSize = (sizeof(arduino.pins) / sizeof(arduino.pins[0])) - 1;
  if (pin_num > arrSize) {
    return C_RED C_BLD + to_string(pin_num) + C_RST;

  } else if (arduino.pins[pin_num]->num.at(0) == '0') {
    return arduino.pins[pin_num]->num.substr(1, 1);
  }
  return arduino.pins[pin_num]->num;
}

// END UTILITY

void pinMode(uint8_t pin_num, uint8_t pin_mode) {
  // storing status for future use
  string pin_mode_status =
      "pinMode(" + Pin2String(pin_num) + ", " +
      ((pin_mode == 0) ? "INPUT"
                       : (pin_mode == 1) ? "INPUT_PULLUP" : "OUTPUT") +
      ");";

  /*  Checks for reasonable arguments: */

  // Is mode either INPUT, INPUT_PULLUP or OUTPUT?
  if (pin_mode > 2) {
    handleError(
        "pinMode(" + to_string(pin_num) + ", " C_RED C_BLD "?" C_RST ");",
        E_PINMODE_MODE, E_PINMODE_EXPL, 0);
  }

  // Does pin at pin_num exist?
  int8_t arrSize = (sizeof(arduino.pins) / sizeof(arduino.pins[0])) - 1;
  if (pin_num > arrSize) {
    handleError(pin_mode_status, E_PIN_DOESNT_EXIST, E_PINMODE_EXPL, 0);
  }

  // Pins with LEDs should only be used as OUTPUT
  if (pin_mode != OUTPUT && arduino.pins[pin_num]->type == LED_t) {
    handleError(pin_mode_status, W_USELESS_INPUT, E_PINMODE_EXPL, 2);

    // Pins on switches and potis should only be used for INPUT or INPUT_PULLUP
    // Voltage on OUTPUT pins is even dangerous!
  } else if (pin_mode == OUTPUT && arduino.pins[pin_num]->type != LED_t) {
    if (arduino.pins[pin_num]->voltage > 0) {
      handleError(pin_mode_status, W_PINMODE_SHORT_CIRCUIT, E_PINMODE_EXPL, 0);
    }
    handleError(pin_mode_status, W_PINMODE_OUTPUT, E_PINMODE_EXPL, 1);
  }

  // Simulating the digital registers in the arduino
  if (pin_mode == OUTPUT && arduino.pins[pin_num]->digital_status == HIGH) {
    arduino.pins[pin_num]->voltage = 1023;
  } else if (pin_mode == INPUT_PULLUP) {
    arduino.pins[pin_num]->digital_status = HIGH;
  } else if (pin_mode == INPUT && arduino.pins[pin_num]->digital_status ==
                                      HIGH) {  // "weird" interaction when
                                               // setting a HIGH pin as INPUT
    pin_mode = INPUT_PULLUP;
  }

  currentCommand(pin_mode_status);
  arduino.pins[pin_num]->mode = pin_mode;
  arduino.display();
}

void digitalWrite(uint8_t pin_num, uint8_t digital_status) {
  string digital_write_status = "digitalWrite(" + Pin2String(pin_num) + ", " +
                                Digital2String(digital_status) + ");";
  currentCommand(digital_write_status);

  /*  Checks for reasonable arguments: */

  // Does pin at pin_num exist?
  int8_t arrSize = (sizeof(arduino.pins) / sizeof(arduino.pins[0])) - 1;
  if (pin_num > arrSize) {
    handleError(digital_write_status, E_PIN_DOESNT_EXIST, E_DIGITALWRITE_EXPL,
                0);
  }

  // Pins used for input shouldn't be changed
  if (arduino.pins[pin_num]->type != LED_t) {
    handleError(digital_write_status, E_DANGEROUS_OUTPUT, E_DIGITALWRITE_EXPL,
                0);
  }

  // Is pinMode set to Input?
  if (arduino.pins[pin_num]->mode != OUTPUT) {
    handleError(digital_write_status, E_MISSING_OUTPUT_MODE,
                E_DIGITALWRITE_EXPL, 1);
    pinMode(pin_num, OUTPUT);
  }

  if (digital_status == HIGH) {
    arduino.pins[pin_num]->voltage = 1023;
  } else if (digital_status == LOW) {
    arduino.pins[pin_num]->voltage = 0;
  } else {
    handleError(digital_write_status, E_DIGITAL_STATUS, E_DIGITALWRITE_EXPL, 0);
  }
  arduino.pins[pin_num]->digital_status = digital_status;
  arduino.display();
}

int digitalRead(uint8_t pin_num) {
  string digital_read_status = "int digitalRead(" + Pin2String(pin_num) + ");";

  /*  Checks for reasonable arguments: */

  // Does pin at pin_num exist?
  int8_t arrSize = (sizeof(arduino.pins) / sizeof(arduino.pins[0])) - 1;
  if (pin_num > arrSize) {
    handleError(digital_read_status, E_PIN_DOESNT_EXIST, E_DIGITALREAD_EXPL, 0);
  }

  // Is pinMode set to Output?
  if (arduino.pins[pin_num]->mode == OUTPUT) {
    handleError(digital_read_status, E_MISSING_INPUT_MODE, E_DIGITALREAD_EXPL,
                1);
    pinMode(pin_num, INPUT);
  }

  // LED Pins could only measure LOW
  if (arduino.pins[pin_num]->type == LED_t) {
    handleError(digital_read_status, W_USELESS_INPUT, E_DIGITALREAD_EXPL, 2);
    return LOW;
  }

  currentCommand(digital_read_status);
  arduino.pins[pin_num]->update(L_CURRENT_COMMAND + digital_read_status);

  if (arduino.pins[pin_num]->voltage > 512) {
    return HIGH;
  }
  return LOW;
}

void analogWrite(uint8_t pin_num, int analog_value) {
  string analog_write_status = "analogWrite(" + Pin2String(pin_num) + ", " +
                               to_string(analog_value) + ");";
  currentCommand(analog_write_status);

  /*  Checks for reasonable arguments: */

  // Does pin at pin_num exist?
  int8_t arrSize = (sizeof(arduino.pins) / sizeof(arduino.pins[0])) - 1;
  if (pin_num > arrSize) {
    handleError(analog_write_status, E_PIN_DOESNT_EXIST, E_ANALOGWRITE_EXPL, 0);
  }

  // valid value for voltage
  if (analog_value < 0 || analog_value > 255) {
    handleError(analog_write_status, E_ANALOGWRITE_OUT_OF_RANGE,
                E_ANALOGWRITE_EXPL, 0);
  }

  // Pins used for input shouldn't be changed
  if (arduino.pins[pin_num]->type != LED_t) {
    handleError(analog_write_status, E_DANGEROUS_OUTPUT, E_ANALOGWRITE_EXPL, 0);
  }

  // that's simply what the Arduno IDE does, too...
  arduino.pins[pin_num]->mode = OUTPUT;

  // Pins from 0 to 3 don't support PWM (analog OUTPUT)

  if (arduino.pins[pin_num]->io != P) {
    handleError(analog_write_status, E_NO_PWM_SUPPORT, E_ANALOGWRITE_EXPL, 2);
    if (analog_value < 128) {
      arduino.pins[pin_num]->digital_status = LOW;
      arduino.pins[pin_num]->voltage = 0;
    } else {
      arduino.pins[pin_num]->digital_status = HIGH;
      arduino.pins[pin_num]->voltage = 1023;
    }

  } else {
    arduino.pins[pin_num]->voltage = analog_value * 4;
  }
  arduino.display();
}

int analogRead(uint8_t pin_num) {
  string analog_read_status = "int analogRead(" + Pin2String(pin_num) + ");";

  /*  Checks for reasonable arguments: */

  // Does pin at pin_num exist?
  int8_t arrSize = (sizeof(arduino.pins) / sizeof(arduino.pins[0])) - 1;
  if (pin_num > arrSize) {
    handleError(analog_read_status, E_PIN_DOESNT_EXIST, E_ANALOGREAD_EXPL, 0);
  }

  // Is pinMode set to Output?
  if (arduino.pins[pin_num]->mode == OUTPUT) {
    handleError(analog_read_status, E_MISSING_INPUT_MODE, E_ANALOGREAD_EXPL, 1);
    pinMode(pin_num, INPUT);
  }

  // Pins from 0 to 7 could only measure 0
  if (arduino.pins[pin_num]->type == LED_t) {
    handleError(analog_read_status, W_USELESS_INPUT, E_ANALOGREAD_EXPL, 2);
    return 0;
  }

  currentCommand(analog_read_status);
  arduino.pins[pin_num]->update(L_CURRENT_COMMAND + analog_read_status);

  return arduino.pins[pin_num]->voltage;
}

// -_-__-___-____-_____TIME_____-____-___-__-_-

unsigned long millis() { return millisSinceStart(); }

unsigned long micros() { return microsSinceStart(); }

void delay(unsigned long delay) {
  string delay_status = "delay(" + to_string(delay) + ");";
  animateStatus(L_CURRENT_COMMAND + delay_status, C_BLC C_BLD BC_WHT,
                delay * 1000);
}

void delayMicroseconds(unsigned int delay) {
  string delay_micros_status = "delayMicroseconds(" + to_string(delay) + ");";
  animateStatus(L_CURRENT_COMMAND + delay_micros_status, C_BLC C_BLD BC_WHT,
                delay);
}

// These functions are used by the arduino-IDE as well
void randomSeed(unsigned long seed) {
  if (seed != 0) {
    srandom(seed);
  }
}

long random(long howbig) {
  if (howbig == 0) {
    return 0;
  }
  return random() % howbig;
}

long random(long howsmall, long howbig) {
  if (howsmall >= howbig) {
    return howsmall;
  }
  long diff = howbig - howsmall;
  return random(diff) + howsmall;
}

long map(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

#endif
