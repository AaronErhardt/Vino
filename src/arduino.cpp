#if defined(__linux__) || defined(_WIN32)
 #include "simulation_lib.hpp"
#ifdef VINO
#define S_PIN 0
#define E_PIN 7
#endif

#ifndef VINO
#define S_PIN 2
#define E_PIN 9
#endif

void setup() {
  Serial.begin(9800);

  for (int x = S_PIN; x <= E_PIN; x++) {
    pinMode(x, OUTPUT);
  }

  Serial.print("Pins are configured now!");
}

void loop() {
  Serial.print("\nLights on!");

  for (int x = S_PIN; x <= E_PIN; x++) {
    digitalWrite(x, HIGH);
    delay(300);
  }
  delay(1000);

  Serial.print("\nLights off!");

  for (int x = S_PIN; x <= E_PIN; x++) {
    digitalWrite(x, LOW);
    //delay(100);
  }
  delay(1000);
}
#endif
