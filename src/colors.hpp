/* colors.hpp: header defining many colors (through escape sequnces)
 *
 * Copyright (c) 2018 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

// general escape codes
#define C_RST "\e[0m"  // reset all colors
#define C_BLD "\e[1m"  // bold font
//#define C_FNT "\e[2m" // faint font
//#define C_ITL "\e[3m" // italic font
//#define C_UDL "\e[4m" // underline
//#define C_OFF "\e[?25l" //cursor off
//#define C_ON "\e[?25h" //cursor on

/* Old font colors
#define C_BLC "\e[30m"    // black
#define C_RED "\e[31m"    // red
#define C_GRN "\e[32m"    // green
#define C_YLW "\e[33m"    // yellow
#define C_BLU "\e[34m"    // blue
#define C_MAG "\e[35m"    // magenta
#define C_CYN "\e[36m"    // cyan
#define C_BR_GRY "\e[37m" // bright grey
#define C_GRY "\e[90m"    // grey
#define C_BR_RED "\e[91m" // bright red
#define C_BR_GRN "\e[92m" // bright green
#define C_BR_YLW "\e[93m" // bright yellow
#define C_BR_BLU "\e[94m" // bright blue
#define C_BR_MAG "\e[95m" // bright magenta
#define C_BR_CYN "\e[96m" // bright cyan
#define C_WHT "\e[97m"   // white
*/
/* Old background colors
#define BC_BLC "\e[40m"     // black
#define BC_RED "\e[41m"     // red
#define BC_GRN "\e[42m"     // green
#define BC_YLW "\e[43m"     // yellow
#define BC_BLU "\e[44m"     // blue
#define BC_MAG "\e[45m"     // magenta
#define BC_CYN "\e[46m"     // cyan
#define BC_BR_GRY "\e[47m"  // bright grey
#define BC_GRY "\e[100m"    // grey
#define BC_BR_RED "\e[101m" // bright red
#define BC_BR_GRN "\e[102m" // bright green
#define BC_BR_YLW "\e[103m" // bright yellow
#define BC_BR_BLU "\e[104m" // bright blue
#define BC_BR_MAG "\e[105m" // bright magenta
#define BC_BR_CYN "\e[106m" // bright cyan
#define BC_WHT "\e[107m"    // white
*/
#define C_TEST "\e[38;2;0;255;0m"
// New colors (should be all) defined by 8-bit RGB values
// Font colors
#define C_BLC "\e[38;2;0;0;0m"      // black
#define C_RED "\e[38;2;255;0;0m"    // red
#define C_GRN "\e[38;2;0;245;20m"   // green
#define C_ARD "\e[38;2;0;151;157m"  // green
//#define C_YLW "\e[33m"               // yellow
#define C_BLU "\e[38;2;50;50;255m"  // blue
#define C_ORG "\e[38;2;255;90;0m"   // orange
//#define C_MAG "\e[35m"               // magenta
//#define C_CYN "\e[36m"               // cyan
#define C_BR_GRY "\e[38;2;170;170;170m"  // bright grey
#define C_GRY "\e[38;2;140;140;140m"     // grey
//#define C_BR_RED "\e[91m"               // bright red
//#define C_BR_GRN "\e[92m"            // bright green
#define C_BR_YLW "\e[38;2;255;255;80m"  // bright yellow
//#define C_BR_BLU "\e[94m"             // bright blue
//#define C_BR_MAG "\e[95m"            // bright magenta
#define C_BR_CYN "\e[38;2;0;240;240m"  // bright cyan
#define C_WHT "\e[38;2;255;255;255m"   // white

// Background colors
//#define BC_BLC "\e[48;2;0;0;0m"     // black
//#define BC_RED "\e[48;2;255;0;0m"     // red
//#define BC_GRN "\e[42m"     // green
//#define BC_YLW "\e[43m"     // yellow
//#define BC_BLU "\e[44m"     // blue
//#define BC_MAG "\e[45m"     // magenta
//#define BC_CYN "\e[46m"     // cyan
//#define BC_BR_GRY "\e[47m"  // bright grey
//#define BC_GRY "\e[100m"    // grey
//#define BC_BR_RED "\e[101m" // bright red
//#define BC_BR_GRN "\e[102m" // bright green
#define BC_BR_YLW "\e[48;2;255;255;0m"  // bright yellow
//#define BC_BR_BLU "\e[104m" // bright blue
//#define BC_BR_MAG "\e[105m" // bright magenta
#define BC_BR_CYN "\e[48;2;0;240;240m"  // bright cyan
#define BC_WHT "\e[48;2;255;255;255m"   // white
