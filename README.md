# ABOUT

Learning to code using the Arduino IDE is a great choice as it provides lots of simple functions and allows to interact easily with a wide range of electric components. Still learning the C/C++ programming language can be challenging, especially when it comes to if-statements and loops. Additionally you can’t just focus on programming because you have to take care of all the electronic components you have connected to your Arduino board. Being inattentive for a moment might even damage the board or other components irreversible. If your program doesn't work as expected you can't always find the cause in the software but a loose contact or wrongly placed LED might be the problem. Also the Arduino software doesn’t inform you about code that compiles correctly but results in nonsense when it gets executed.
Here's where Vino becomes handy: It will use the same code but runs it in a virtual environment with integrated LEDs, switches and potentiometers. The software even warns you or stops the program automatically if you're accidentally doing something wrong and gives you a hint what’s the problem and how to fix it. Arduino hardware isn’t needed, too, so you can simply start coding in case you haven’t a board available. Further features are planed to provide even more detailed information about errors and Arduino functions. An additional program helping you to learn programming from scratch will be added soon as well.

## Main Features

-   a native C/C++ environment
-   support for all essential Arduino functions
-   advanced real-time bug and nonsense tracking
-   a neat console interface with 24bit colors

# INSTALL

1.  Download the zip file and extract it.  
2.  Right-click on the folder and select 'open in terminal'  
    (optionally you can open a terminal and enter 'cd /PATH/TO/FOLDER')
3.  Now type 'bash install.sh' and follow the instructions (this may need root privileges)
4.  You should now have a desktop shortcut and a menu entry to start vino!
5.  To edit the program type "edit" or open the Vino.ino file inside a text editor
6.  Type "help" to get more information about further Vino commands

NOTE: It's recommended to set up the Arduino IDE as well. They both work well together and so it's recommended to use the Arduino IDE as editor for Vino.

Information for developers and beta testers are provided in the README-dev file.

# Version

BETA: 0.4.0

Should you notice a bug, crash, spelling mistake or anything related or should you have an idea for new features or improvements let me know it! Simply contact me at aaron.erhardt@t-online.de.
