# update.sh
#
# Copyright (c) 2019 Aaron Erhardt
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

SCRIPT_PATH=$( cd $(dirname $0) ; pwd )
cd "${SCRIPT_PATH}"
cd ..

echo "> creating temporary directory"

mkdir tmp

cp etc/release_number tmp/release_number_old

cd tmp/

echo "> looking up if a new version is available"

wget=`command -v wget`
empty=""

if [[ $wget == $empty ]] ; then
  wget https://gitlab.com/AaronErhardt/Vino/raw/master/etc/release_number
else
  curl -O https://gitlab.com/AaronErhardt/Vino/raw/master/etc/release_number
fi

new=`cat release_number`
old=`cat release_number_old`

if [[ $new == $old ]] ; then
  echo "Your version of Vino is up to date!"
  cd ..
  rm -r tmp/
  exit 0
fi

echo "> downloading new version"

if [[ $wget == $empty ]] ; then
  wget https://gitlab.com/AaronErhardt/Vino/-/archive/master/Vino-master.tar
else
  curl -O https://gitlab.com/AaronErhardt/Vino/-/archive/master/Vino-master.tar
fi

echo "> extracting files"

tar -xf Vino-master.tar

echo "> replacing old files"

cp -r Vino-master/* ../

language=`grep "language" ../etc/vino.config`

if [ "$language" == "language=german" ]; then
  cp ../etc/lang/german.h ../src/language.hpp
else
  cp ../ect/lang/english.h ../src/language.hpp
fi

echo "> compiling Vino"

cd ../src/

make clean
make vino

rm ../vino
mv vino ../vino

echo "> cleaning up"

cd ..

rm -r tmp/

echo "> finished! (restart Vino to apply updates)"
