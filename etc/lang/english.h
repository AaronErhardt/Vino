// english.h
/* header for English language support
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Str// german.h
eet, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* Meaning of the first letter:
 * L: usual language constant
 * W: warning
 * E: error
 * I: information
 * V: value
 * C: command
 */

// Texts for vino.cpp
#define L_VINO_WELCOME "Welcome to \e[38;2;255;0;0m\e[1mVino\e[0m"
#define W_UNKNOWN_COMMAND "Unknown command!"
#define L_VINO_TYPE_HELP "Type 'help' to get more information"
#define L_VINO_BYE "bye!"
#define L_VINO_HELP \
  "\e[38;2;255;0;0m\e[1mVino\e[0m\e[1m help page\e[0m\n\
 - open:     open a project from the sketchbook folder\n\
 - edit:     open a text editor (other editors can be used as well)\n\
 - compile:  compile a Vino program\n\
 - run:      run a Vino program (compile first!)\n\
 - autorun:  compile and run\n\
 - list:     list all available commands\n\
 - errors:   prints the error log file\n\
 - serial:   prints the serial log file\n\
 - help:     this help page\n\
 - version:  prints Vino version\n\
 - update:   update Vino to the latest version\n\
 - clean:    clean program files (to re-compile the whole program)\n\
 - exit \n\
   or quit:  leave Vino\n\
HINT: press TAB to autocomplete commands\n\
      press 1 or 2 to move in the command history!"

#define C_AUTORUN "autorun"
#define C_CLEAN "clean"
#define C_COMPILE "compile"
#define C_EDIT "edit"
#define C_ERRORS "errors"
#define C_EXIT "exit"
#define C_HELP "help"
#define C_LIST "list"
#define C_QUIT "quit"
#define C_SERIAL "serial"
#define C_RUN "run"
#define C_UPDATE "update"
#define C_VERSION "version"
#define C_OPEN "open"

#define L_NOTE_EDIT_MANUALLY \
  "Note that you can edit files outside Vino as well!"

// Texts for main.cpp
#define L_PROGRAM_STOPPED "Program stopped!"
#define L_HEADING "Vino"
#define W_UNEXPECTED_EXIT "The program stopped unexpectedly!"
#define L_CTR_C "Ctr+C"

// Texts for console_util.*
#define E_FILE_OPEN "Couldn't open file!"
#define E_DIR_OPEN "Couldn't open directory!"

// Texts for setup_console.*
#define L_RESIZE_ROWS \
  "Please resize your console window so it contains at least 35 rows!"
#define L_SHOULD_SEE_FROM_BOTTOM \
  "You should still see this if you scrolled down to the bottom!"

#define L_RESIZE_COLUMNS \
  "Please resize your console window so it contains at least 83 columns!"
#define L_FIT_IN_SINGLE_LINE                                                   \
  "   This       text       should       fit       in       a       single   " \
  "    line!"

// Texts for serial_lib.*
#define L_SERIAL_ESTABLISHED "Serial communication established with "
#define E_SERIALBEGIN_EXPL "Serial.begin(9800);"
#define E_ESTABLISH_SERIAL_COM_EXPL \
  "Use Serial.begin(9800); first e.g. in void setup!"

#define E_SERIALBEGIN_OUT_OF_RANGE "baud is out of range!"
#define E_SERIAL_COM_NOT_ACTIVE "No serial connection established yet!"

// Texts for error.*
#define L_ISSUE "ISSUE: "
#define L_EXAMPLE "CORRECT EXAMPLE: "
#define E_CRITICAL "CRITICAL ERROR: "
#define E_CRITICAL_EXPL "The program was stopped automatically!"
#define E "ERROR: "
#define E_EXPL                                                           \
  "The program will continue shortly! Use \e[38;2;0;240;240mCtr+C\e[0m " \
  "to cancel! "
#define I "INFO: "
//#define I_EXPL "For more information type \"help\" and search for the
// command!"
#define I_EXPL \
  "For more information search for the command in the arduino reference."
#define L_WARNING "WARNING: "

// Texts for sim_util.*
#define L_NO_CHANGES "no changes"
#define L_INPUT_REQUIRED "Input required"
#define L_SHORT_YES 'y'
#define L_SHORT_NO 'n'
#define L_ON "on"
#define L_OFF "off"
#define L_RESULT "Result"
#define L_ENTER_NEW_VALUE "Enter a new value"
#define L_OUT_OF_RANGE "out of range"
#define L_NO_VALID_INPUT "No valid input!"

// Texts for sim_lib.*
#define L_LED_SPACING "   "
#define L_LED "LED"
#define L_SWITCH "SWITCH"
#define L_POTENTIOMETER "POTENTIOMETER"
#define V_POTENTIOMETER_LETTERS 13
#define L_CURRENT_COMMAND " Current command: "

// Explanations
#define E_PINMODE_EXPL "pinMode(2, OUTPUT);"
#define E_DIGITALWRITE_EXPL "digitalWrite(3, HIGH);"
#define E_DIGITALREAD_EXPL "int num = digitalRead(10);"
#define E_ANALOGWRITE_EXPL "analogWrite(5, 125);"
#define E_ANALOGREAD_EXPL "int val = analogRead(A1);"
#define E_DELAY_EXPL "delay(1000);"
#define E_DELAYMICROSECONDS_EXPL "delayMicroseconds(900);"

#define E_PINMODE_MODE "Invalid mode: use INPUT, INPUT_PULLUP or OUTPUT!"
#define E_PIN_DOESNT_EXIST "This Pin doesn't exist!"
#define W_USELESS_INPUT \
  "The selected pin can't measure any input, the voltage won't change!"
//#define W_PINMODE_INPUT_PULLUP
//  "Pins defined as INPUT_PULLUP are set to +5V by default!"
#define W_PINMODE_OUTPUT \
  "Do you really want to risk a short circuit at this pin?"
#define W_PINMODE_SHORT_CIRCUIT \
  "Voltage on an output pin will create short circuits!"

#define E_MISSING_INPUT_MODE \
  "The selected pin is not set to INPUT nor INPUT_PULLUP!"
#define E_MISSING_OUTPUT_MODE "The selected pin is not set to OUTPUT!"
#define E_DANGEROUS_OUTPUT "This action will lead to a short circuit!"
#define E_DIGITAL_STATUS "Invalid input: neither HIGH nor LOW"
#define E_NO_PWM_SUPPORT "PWM (analogWrite) is not supported on this pin!"
#define E_ANALOGWRITE_OUT_OF_RANGE \
  "Only values from 0 (0V) to 255 (5V) can be used!"

// Texts for file.*
#define L_BROKEN_CONFIG "Broken config file! (vino.config)"
#define L_SELECT_PROJECT "Select the project you want to open:"
#define E_NUM_CANT_BE_SELECTED "This number can't be selected!"
#define L_SELECTED_PROJECT "Selected project: "
#define E_COULDNT_BE_OPENED_1 "Project file at "
#define E_COULDNT_BE_OPENED_2 " couldn't be opened!"
