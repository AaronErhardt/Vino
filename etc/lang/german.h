// german.h
/* header for German language support
 *
 * Copyright (c) 2018-2019 Aaron Erhardt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

// Texte für vino.cpp
#define L_VINO_WELCOME "Willkommen in \e[38;2;255;0;0m\e[1mVino\e[0m"
#define W_UNKNOWN_COMMAND "Unbekannter Befehl!"
#define L_VINO_TYPE_HELP "Verwende 'hilfe' für mehr Informationen"
#define L_VINO_BYE "Auf Wiedersehen!"
#define L_VINO_HELP \
  "\e[38;2;255;0;0m\e[1mVino\e[0m\e[1m Hilfsseite\e[0m\n\
 - oeffnen:       öffnet ein Projekt aus dem sketchbook Ordner\n\
 - bearbeiten:    öffnet einen Texteditor\n\
 - kompilieren:   kompiliert ein Vino-Programm\n\
 - ausfuehren:    führt ein Vino-Programm aus (zuerst kompileren!)\n\
 - autostart:     kompiliert ein Vino-Programm und führt es danach aus\n\
 - listen:        listet alle verfügbaren Befehle auf\n\
 - error:         zeigt die Error Logdatei an\n\
 - serial:        zeigt die Serial Logdatei an\n\
 - hilfe:         zeigt diese Hilfsseite an\n\
 - version:       zeigt die Programmversion an\n\
 - aktualisieren: aktualisiert Vino auf die beuste Version\n\
 - bereinigen:    bereinigt alle Kompilerdateien (um alles neu zu kompilieren)\n\
 - beenden oder\n\
   verlassen:     Vino beenden\n\
HINWEIS: drücke TAB zum automatischen Vervollständigen von Befehlen\n\
         drücke 1 oder 2 um auf den Befehlsverlauf zuzugreifen!"

#define C_AUTORUN "autostart"
#define C_CLEAN "bereinigen"
#define C_COMPILE "kompilieren"
#define C_EDIT "bearbeiten"
#define C_ERRORS "error"
#define C_EXIT "beenden"
#define C_HELP "hilfe"
#define C_LIST "listen"
#define C_QUIT "verlassen"
#define C_SERIAL "serial"
#define C_RUN "ausfuehren"
#define C_UPDATE "aktualisieren"
#define C_VERSION "version"
#define C_OPEN "oeffnen"

#define L_NOTE_EDIT_MANUALLY \
  "Du kannst Dateien übrigens auch unabhängig von Vino bearbeiten!"

// Texts for main.cpp
#define L_PROGRAM_STOPPED "Programm gestoppt!"
#define L_HEADING "Vino"
#define W_UNEXPECTED_EXIT "Das Programm wurde unerwartet beendet!"
#define L_CTR_C "Str+C"

// Texts for console_util.*
#define E_FILE_OPEN "Datei konnte nicht geöffnet werden!"
#define E_DIR_OPEN "Ordner konnte nicht geöffnet werden!"

// Texts for setup_console.*
#define L_RESIZE_ROWS \
  "Bitte vergrößern Sie die Konsole, sodass sie mindestens 35 Reihen enthält!"
#define L_SHOULD_SEE_FROM_BOTTOM \
  "Du solltest diesen Text noch sehen, wenn ganz nach untern scrollst!"

#define L_RESIZE_COLUMNS                                                  \
  "Bitte vergrößern Sie die Konsole, sodass sie mindestens 83 Spalten " \
  "enthält!"
#define L_FIT_IN_SINGLE_LINE                                                   \
  "Dieser     Text    sollte     noch     in     eine     einzige     Zeile  " \
  "  passen!"

// Texts for serial_lib.*
#define L_SERIAL_ESTABLISHED "Serielle Kommunikation gestartet mit "
#define E_SERIALBEGIN_EXPL "Serial.begin(9800);"
#define E_ESTABLISH_SERIAL_COM_EXPL \
  "Benutze Serial.begin(9800) zuerst z.B. in void setup!"
/*#define E_SERIALBEGIN_MISSING_INT
"Serial.begin(int) needs one argument of type int!" // should be added soon!!!!!
*/
#define E_SERIALBEGIN_OUT_OF_RANGE "Baud zu hoch oder zu niedrig!"
#define E_SERIAL_COM_NOT_ACTIVE "Serielle Kommunikation noch nicht aktiv!"
// Texts for error.*
#define L_ISSUE "PROBLEM: "
#define L_EXAMPLE "RICHTIGES BEISPIEL: "
#define E_CRITICAL "KRITISCHER FEHLER: "
#define E_CRITICAL_EXPL "Das Programm wurde automatisch gestoppt!"
#define E "FEHLER: "
#define E_EXPL                                                               \
  "Das Programm läuft gleich weiter! Drücke \e[38;2;0;240;240mStr+C\e[0m " \
  "zum Abbrechen! "
#define I "INFO: "
//#define I_EXPL "Gib \"hilfe\" ein für mehr Informationen und suche nach dem
// Befehl!"
#define I_EXPL \
  "Für mehr Informationen suche nach dem Befehl in der Arduino-Dokumentation."
#define L_WARNING "WARNUNG: "

// Texts for sim_util.*
#define L_NO_CHANGES "keine Änderungen"
#define L_INPUT_REQUIRED "Eingabe benötigt"
#define L_SHORT_YES 'j'
#define L_SHORT_NO 'n'
#define L_ON "an"
#define L_OFF "aus"
#define L_RESULT "Ergebnis"
#define L_ENTER_NEW_VALUE "Gib einen neuen Wert ein"
#define L_OUT_OF_RANGE "Wert zu groß"
#define L_NO_VALID_INPUT "Keine gültige Eingabe!"

// Texts for sim_lib.*
#define L_LED_SPACING "   "
#define L_LED "LED"
#define L_SWITCH "SCHALTER"
#define L_POTENTIOMETER "POTENTIOMETER"
#define V_POTENTIOMETER_LETTERS 13
#define L_CURRENT_COMMAND " Aktueller Befehl: "

// Explanations
#define E_PINMODE_EXPL "pinMode(2, OUTPUT);"
#define E_DIGITALWRITE_EXPL "digitalWrite(3, HIGH);"
#define E_DIGITALREAD_EXPL "int num = digitalRead(10);"
#define E_ANALOGWRITE_EXPL "analogWrite(5, 125);"
#define E_ANALOGREAD_EXPL "int val = analogRead(A1);"
#define E_DELAY_EXPL "delay(1000);"
#define E_DELAYMICROSECONDS_EXPL "delayMicroseconds(900);"

#define E_PINMODE_MODE \
  "Ungültiger Wert: benutze INPUT, INPUT_PULLUP oder OUTPUT!"
#define E_PIN_DOESNT_EXIST "Dieser Pin existiert nicht!"
#define W_USELESS_INPUT \
  "Die Spannung an dem ausgewählten Pin kann sich nicht ändern!"
//#define W_PINMODE_INPUT_PULLUP
//  "Pins defined as INPUT_PULLUP are set to +5V by default!"
#define W_PINMODE_OUTPUT \
  "Output an diesem Pin könnte zu einen Kurzschluss führen."
#define W_PINMODE_SHORT_CIRCUIT \
  "Spannung an einem Output-Pin kann Kurzschlüsse erzeugen!"

#define E_MISSING_INPUT_MODE \
  "Der ausgewählte Pin ist nicht als INPUT oder INPUT_PULLUP gesetzt!"
#define E_MISSING_OUTPUT_MODE \
  "Der ausgewählte Pin ist nicht als OUTPUT gesetzt!"
#define E_DANGEROUS_OUTPUT \
  "Spannung an diesem Pin würde zu einem Kurzschluss führen!"
#define E_DIGITAL_STATUS "Ungültige Eingabe: weder HIGH noch LOW"
#define E_NO_PWM_SUPPORT \
  "PWM (analogWrite) wird auf diesem Pin nicht unterstützt!"
#define E_ANALOGWRITE_OUT_OF_RANGE \
  "Nur Werte von 0 (0V) bis 255 (5V) können benutzt werden!"

// Texts for file.*
#define L_BROKEN_CONFIG "Fehlerhafte Konfigurationsdatei! (vino.config)"
#define L_SELECT_PROJECT "Wähle das Projekt aus, das du öffnen möchtest:"
#define E_NUM_CANT_BE_SELECTED "Diese Nummer kann nicht ausgewählt werden!"
#define L_SELECTED_PROJECT "Ausgewähltes Projekt: "
#define E_COULDNT_BE_OPENED_1 "Projektdatei "
#define E_COULDNT_BE_OPENED_2 " konnte nicht geöffnet werden!"
