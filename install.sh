# install.sh
#
# Copyright (c) 2018-2019 Aaron Erhardt
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# THIS FILE IS BASED ON THE ARDUINO IDE INSTALLER FOR LINUX

#!/bin/bash

SCRIPT_PATH=$( cd $(dirname $0) ; pwd )
cd "${SCRIPT_PATH}"

function installDesktopIcons {
 RESOURCE_NAME=vino-vino

 printf "> Adding desktop shortcut, menu item and file associations for Vino..."

 # Create a temp dir accessible by all users
 TMP_DIR=`mktemp --directory`

 # Create *.desktop file using the existing template file
 sed -e "s,<BINARY_LOCATION>,${SCRIPT_PATH}/vino,g" \
  -e "s,<DIR_LOCATION>,${SCRIPT_PATH},g" \
  -e "s,<ICON_NAME>,${RESOURCE_NAME},g" "${SCRIPT_PATH}/etc/desktop.template" > "${TMP_DIR}/${RESOURCE_NAME}.desktop"

 # Install the icon files using name and resolutions
 xdg-icon-resource install --context apps --size 16 "${SCRIPT_PATH}/etc/icons/vino16.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 24 "${SCRIPT_PATH}/etc/icons/vino24.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 32 "${SCRIPT_PATH}/etc/icons/vino32.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 48 "${SCRIPT_PATH}/etc/icons/vino48.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 64 "${SCRIPT_PATH}/etc/icons/vino64.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 72 "${SCRIPT_PATH}/etc/icons/vino72.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 96 "${SCRIPT_PATH}/etc/icons/vino96.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 128 "${SCRIPT_PATH}/etc/icons/vino128.png" $RESOURCE_NAME
 xdg-icon-resource install --context apps --size 256 "${SCRIPT_PATH}/etc/icons/vino256.png" $RESOURCE_NAME

 # Install the created *.desktop file
 xdg-desktop-menu install "${TMP_DIR}/${RESOURCE_NAME}.desktop"

 # Create icon on the desktop
 xdg-desktop-icon install "${TMP_DIR}/${RESOURCE_NAME}.desktop"

 # Clean up
 rm "${TMP_DIR}/${RESOURCE_NAME}.desktop"
 rmdir "$TMP_DIR"

 printf " done!\n"

}

echo -e "\x1b[1m  --- \e[38;2;176;0;0mVino\x1b[0m \x1b[1minstallation --- \x1b[0m"

PKGM=" "
install_manually="Please install make and g++ manually."
empty=""

function findPKGM {

  apt=`command -v apt`
  aptget=`command -v apt-get`
  dnf=`command -v dnf`
  yum=`command -v yum`
  pacman=`command -v pacman`

  if [ -n $pacman ] && [[ $pacman != $empty ]] ; then
    PKGM="pacman"
  elif [ -n $dnf ] && [[ $dnf != $empty ]] ; then
    PKGM="dnf"
  elif [ -n $yum ] && [[ $yum != $empty ]] ; then
    PKGM="yum"
  elif [ -n $apt ] && [[ $apt != $empty ]]; then
    PKGM="apt"
  elif [ -n $aptget ] && [[ $aptget != $empty ]] ; then
    PKGM="apt-get"
  else
    PKGM="null"
    echo "> No package manager found! " $install_manually
    saveExit
  fi

}

function installPKG {

  if [[ $PKGM = " " ]] ; then
    findPKGM
  fi

  if [[ $PKGM = "pacman" ]] ; then
    sudo pacman -S $1
  elif [[ $PKGM = "dnf" ]] ; then
    sudo dnf install $1
  elif [[ $PKGM = "yum" ]] ; then
    sudo yum install $1
  elif [[ $PKGM = "apt" ]] ; then
    sudo apt install $1 -y
  elif [[ $PKGM = "apt-get" ]] ; then
    sudo apt-get install $1 -y
  else
    echo "> Couldn't install the package! " $install_manually
  fi

}

function installDependencies {

  gxx=`command -v g++`
  make=`command -v make`

  if [[ $gxx == $empty ]] ; then
    echo -e "> \x1b[1mg++\x1b[0m is not installed"
    confirm "> Do you want to install necessary packages? [y/N] "
    installPKG g++
  fi

  gxx=`command -v g++`

  if [[ $gxx == $empty ]] ; then
    echo -e "\x1b[1m\e[38;2;200;0;0m>\x1b[0m Installation of \x1b[1mg++\x1b[0m failed"
    exit 1
  fi


  if [[ $make == $empty ]] ; then
    echo -e "> \x1b[1mmake\x1b[0m is not installed"
    confirm "> Do you want to install the necessary packages? [y/N] "
    installPKG make
  fi

  make=`command -v make`

  if [[ $make == $empty ]] ; then
    echo -e "\x1b[1m\e[38;2;200;0;0m>\x1b[0m Installation of \x1b[1mmake\x1b[0m failed"
    exit 1
  fi

}

function enableUpdates {

  wget=`command -v wget`
  curl=`command -v curl`
  tar=`command -v tar`

  if [[ $wget == $empty ]] ; then
    if [[ $curl == $empty ]] ; then
    echo -e "> Neither \x1b[1mwget\x1b[0m nor \x1b[1mcurl\x1b[0m is installed"
    confirm "> Do you want to install the necessary packages? [y/N] "
    installPKG wget
    fi
  fi

  if [[ $tar == $empty ]] ; then
  echo -e "> \x1b[1mtar\x1b[0m is not installed"
  confirm "> Do you want to install the necessary packages? [y/N] "
  installPKG tar
  fi
}

function confirm {

  read -r -p "$1" response
  case "$response" in
    [yY][eE][sS]|[yY])
    true
    ;;
  *)
    saveExit
    ;;
  esac
}

function saveExit {
  echo -e "\x1b[1m  --- Installation has been stopped --- \x1b[0m"
  exit 0
}

sketchbook=$HOME"/sketchbook/"

read -r -p "Is $sketchbook your sketchbook folder? [y/N] " response
    case "$response" in
        [nN][oO]|[nN])
            echo -e "Please enter your sketchbook folder path! \nThe path can be found in the prefernces of Arduino IDE."
            read -e response
            sketchbook="$response"
            ;;
        *)
    esac

if [ "$sketchbook" == "" ]; then
  echo "Empty sketchbook folder is not allowed"
  saveExit
fi

echo "sketchbook=$sketchbook" > etc/vino.config

echo "> Adding Vino Project to sketchbook"

mkdir -p $sketchbook"Vino/"
cp Vino.ino $sketchbook"Vino/Vino.ino"
echo "project=Vino" >> etc/vino.config

echo -e "Which languge do you want to use?\n1: English\n2: Deutsch"
read -r response
if [ "$response" == "2" ]; then
  echo -e "Deutsch selected"
  echo "language=german" >> etc/vino.config
  cp etc/lang/german.h src/language.hpp
else
  echo -e "English selected"
  echo "language=english" >> etc/vino.config
  cp etc/lang/english.h src/language.hpp
fi

read -r -p "Do you prefer a specific editor? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            echo -e "Enter the command that starts the editor (usually its name):"
            read -e response
            echo "editor="$response >> etc/vino.config
            ;;
        *)
            echo "editor=nano" >> etc/vino.config
            ;;
    esac

installDependencies

echo -e "> building the program"
cd src/
make vino
cd ..
mv src/vino vino

read -r -p "Do you want to install a desktop shortcut? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            installDesktopIcons
            ;;
        *)
            ;;
    esac

read -r -p "Do you want to enable the updater? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            enableUpdates
            ;;
        *)
            ;;
    esac

echo -e "\x1b[1m  --- Everything is installed! --- \x1b[0m"

read -r -p "Do you want to start Vino now? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY])
            ./vino
            ;;
        *)
            exit
            ;;
    esac
