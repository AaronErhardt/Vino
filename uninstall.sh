# uninstall.sh
#
# Copyright (c) 2019 Aaron Erhardt
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# THIS FILE IS BASED ON THE ARDUINO IDE INSTALLER FOR LINUX

RESOURCE_NAME=vino-vino

# Get absolute path from which this script file was executed
SCRIPT_PATH=$( cd $(dirname $0) ; pwd )
cd "${SCRIPT_PATH}"

xdg-desktop-menu uninstall ${RESOURCE_NAME}.desktop

xdg-desktop-icon uninstall ${RESOURCE_NAME}.desktop

xdg-icon-resource uninstall --size 16 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 24 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 32 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 48 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 64 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 72 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 96 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 128 ${RESOURCE_NAME}
xdg-icon-resource uninstall --size 256 ${RESOURCE_NAME}
